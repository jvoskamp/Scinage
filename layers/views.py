from django.shortcuts import render, get_object_or_404, Http404
from django.http import JsonResponse, HttpResponse, HttpResponseRedirect
from django.utils.html import escape
from django.views.decorators.clickjacking import xframe_options_exempt
from django.views.decorators.vary import vary_on_cookie
from models import InputType, Version, Layer, Control, SlideLayer, FavoriteLayer, UploadedImage, Screen, GroupMembership
from django.contrib.auth.models import User
from django.db.models import Count
from forms import *
from functions import get_item_permissions
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from django.forms.models import inlineformset_factory
from jinja2 import Template
import json
from helpers.extra import extra_globals
from datetime import datetime
# Create your views here.


@login_required
@vary_on_cookie
def frontpage(request):
    if not request.user.is_authenticated():
        return render(request, 'UI/frontpage-nologin.html')
    else:
        # Get all this person's shit
        owned_groups = request.user.owned_groups.all()
        other_groups = request.user.contentgroup_set.all()
        all_groups = owned_groups | other_groups
        all_groups = all_groups.distinct()

        owned_layers = request.user.layer_set.all()
        shared_layers = owned_layers.filter(shared=True)
        layer_uses = SlideLayer.objects.filter(version__layer__in=shared_layers).count()
        other_layers = sum([x.layers.all().exclude(owner=request.user).count() for x in all_groups])
        fav_layers = request.user.favoritelayer_set.all().count()

        owned_slides = request.user.slide_set.all().count()
        other_slides = sum([x.slides.all().exclude(owner=request.user).count() for x in all_groups])

        owned_slideshows = request.user.slideshow_set.all().count()
        other_slideshows = sum([x.slideshows.all().exclude(owner=request.user).count() for x in all_groups])

        context = {'owned_groups': owned_groups,
                   'other_groups': other_groups,
                   'owned_layers': owned_layers.count(),
                   'other_layers': other_layers,
                   'fav_layers': fav_layers,
                   'shared_layers': shared_layers.count(),
                   'layer_uses': layer_uses,
                   'owned_slides': owned_slides,
                   'other_slides': other_slides,
                   'owned_slideshows': owned_slideshows,
                   'other_slideshows': other_slideshows,
                   }
        return render(request, 'UI/frontpage.html', context)


def ajaximage(request):
    if request.method == "POST":
        try:
            image = request.FILES.get('image')
            uim = UploadedImage.objects.create(image=image, owner=request.user)
            uim.save()
            info = {'thumb': uim.thumbnail.url, 'image': uim.image.url}
            return JsonResponse(info, safe=False)
        except Exception, e:
            print str(e)
            return JsonResponse("Failed to upload image (%s)" % str(e), safe=False)
    return JsonResponse("Not Post", safe=False)


# Uses AJAX to add or remove layers from a user's favorites
@login_required
def toggleLayerFav(request):
    if request.method != 'POST':
        raise Http404
    layer = get_object_or_404(Layer, id=int(request.POST['id']))
    fav = FavoriteLayer.objects.filter(user=request.user, layer=layer).first()
    if fav is None:
        FavoriteLayer.objects.create(user=request.user, layer=layer)
        return HttpResponse('ADDED')
    else:
        fav.delete()
        return HttpResponse('REMOVED')


@login_required
def controlsExtra(request):
    if not request.user.is_authenticated():
        raise Http404

    context = {'images': request.user.uploadedimage_set.order_by('-creation_date')}
    return render(request, 'layers/extra_control_selectors.html', context)


@login_required
def cloneLayer(request, layer_id):
    layer = get_object_or_404(Layer, pk=layer_id)
    # NEED TO VERIFY USER HAS VIEW PERMISSION!
    newlayer = layer.clone(request.user)
    return HttpResponseRedirect('/layers/edit/%s/' % newlayer.id)


@login_required
@vary_on_cookie
def manageLayers(request):
    # for now get all official layers and all user layers
    return render(request, 'layers/layers.html')


@login_required
@vary_on_cookie
def listLayers(request, mode):
    layers = Layer.objects.filter(owner=request.user).order_by('title')
    groups = request.user.contentgroup_set.all() | request.user.owned_groups.all()
    groups = groups.distinct().annotate(c=Count('layers')).filter(c__gt=0)
    # This isn't going to work if people edit eachother's stuff... Also wayyy too slow
    popular = Layer.objects.filter(shared=True).annotate(num_fav=Count('favoritelayer')).order_by('num_fav')[:5]
    # popular = Layer.objects.filter(shared=True)
    recent = list(set([x.version.layer for x in SlideLayer.objects.filter(version__layer__owner=request.user).reverse()]))[:4]
    favorites = set([x.layer for x in FavoriteLayer.objects.filter(user=request.user)])
    return render(request, 'layers/layer_list.html', {'popular': popular, 'layers': layers, 'mode': mode, 'groups': groups, 'recent': recent, 'favorites': favorites})


# Create or Edit Layers.
@login_required
@vary_on_cookie
def editLayer(request, layer_id=None):
    prev = request.GET.get('preview', False)
    savestatus = None
    layer = None if layer_id is None else get_object_or_404(Layer, pk=layer_id)

    # Verify the user can edit this
    if layer is not None:
        if not get_item_permissions(request.user, layer).get('edit', False):
            raise Http404

    version = None if layer is None else layer.version_set.order_by('-creation_date')[0:1].get()
    layerform = LayerForm() if layer is None else LayerForm(instance=layer)
    formset = inlineformset_factory(Version, Control, form=ControlForm, can_delete=True, extra=0)
    thisformset = formset(prefix="controls") if layer is None else formset(prefix="controls", instance=version)
    versionform = VersionForm() if layer is None else VersionForm(instance=version)

    if request.method == 'POST':

        thisformset = formset(data=request.POST, prefix="controls") if layer is None else formset(instance=version, data=request.POST, prefix='controls')
        layerform = LayerForm(request.POST, request.FILES) if layer is None else LayerForm(request.POST, request.FILES, instance=layer)
        versionform = VersionForm(data=request.POST) if layer is None else VersionForm(instance=version, data=request.POST)

        if prev is not False:
            thisformset.is_valid() and versionform.is_valid()

            try:
                template = Template(versionform.cleaned_data['content'])
                template.globals.update(extra_globals)
                variables = {}
                for form in thisformset:
                    if form.cleaned_data['DELETE'] is False:
                        variables[form.cleaned_data['variable_name']] = form.cleaned_data['default']
                preview = template.render(**variables)
            except Exception, e:
                preview = "Failed to render preview. Error: %s" % str(e)
            resp = HttpResponse(preview)
            resp.__setitem__('X-XSS-Protection', '0')
            return resp

        else:
            if thisformset.is_valid() and layerform.is_valid() and versionform.is_valid():
                request.session['newlysaved'] = True
                # print layerform.cleaned_data['icon']
                if layer_id is None:
                    newlayer = layerform.save(commit=False)
                    if newlayer.id is None:
                        newlayer.owner = request.user
                    newlayer.save()
                    layerform.save_m2m()

                    newversion = versionform.save(commit=False)
                    newversion.layer = newlayer
                    newversion.changes = "First Version."
                    newversion.save()

                    instances = thisformset.save(commit=False)
                    for obj in thisformset.deleted_objects:
                        obj.delete()
                    for instance in instances:
                        instance.version = newversion
                        instance.save()
                    return HttpResponseRedirect('/layers/edit/%s/' % newlayer.id)
                else:
                    layerform.save()
                    versionform.save()
                    instances = thisformset.save()
                    for instance in instances:
                        print instance
                    return HttpResponseRedirect('/layers/edit/%s/' % layer.id)
            else:
                print versionform.errors, thisformset.errors, layerform.errors

    context = {'layerform': layerform,
               'versionform': versionform,
               'controlsform': thisformset,
               'helper': ControlsInlineHelper(),
               'trhelper': ControlsRowHelper(),
               'layer': layer,
               'version': version,
               }
    return render(request, 'layers/editlayer.html', context)


@login_required
@vary_on_cookie
def loadInput(request, id, force_input_id="", force_input_name="", control_id=None):
    thisinput = get_object_or_404(InputType, pk=id)
    if control_id is None:
        return HttpResponse(Template(thisinput.html).render(input_id=force_input_id, input_name=force_input_name))


@csrf_exempt
def layerSettings(request, version_id):
    if request.method != "POST":
        raise Http404
    version = get_object_or_404(Version, pk=version_id)
    controls = []
    for control in version.control_set.all():
        template = Template(control.input_type.html)

        controls.append(template.render(control_title=control.title, control_verbose=control.verbose, control_variable=control.variable_name, control_value=request.POST.get(control.variable_name, control.default)))
    return render(request, 'slides/layersettings.html', {'controls': controls})


@login_required
def cloneSlide(request, slide_id):
    slide = get_object_or_404(Slide, pk=slide_id)
    # NEED TO VERIFY USER HAS VIEW PERMISSION!
    newslide = slide.clone(request.user)
    return HttpResponseRedirect('/slides/edit/%s/' % newslide.id)


@login_required
@vary_on_cookie
def manageSlides(request):
    # for now get all official layers and all user layers
    return render(request, 'slides/slides.html')


@login_required
@vary_on_cookie
def listSlides(request, mode):
    slides = Slide.objects.filter(owner=request.user).order_by('-edit_date')
    groups = request.user.contentgroup_set.all() | request.user.owned_groups.all()
    groups = groups.distinct().annotate(c=Count('slides')).filter(c__gt=0)
    return render(request, 'slides/slide_list.html', {'slides': slides, 'groups': groups, 'mode': mode})


# Create your views here.
@login_required
@vary_on_cookie
def editSlide(request, slide_id=None):
    prev = request.GET.get('preview', False)
    # Keep track of if we saved or not..
    savestatus = None

    slide = None if slide_id is None else get_object_or_404(Slide, pk=slide_id)

    # Verify the user can edit this
    if slide is not None:
        if not get_item_permissions(request.user, slide).get('edit', False):
            raise Http404

    form = SlideForm() if slide is None else SlideForm(instance=slide)
    slidelayer_factory = inlineformset_factory(Slide, SlideLayer, form=SlideLayerForm, can_delete=True, extra=0)
    slidelayerset = slidelayer_factory(prefix="layer") if slide is None else slidelayer_factory(prefix="layer", instance=slide)

    if request.method == "POST":
        # Assume save failed unless proven otherwise.
        savestatus = False

        # Load data into forms...
        slidelayerset = slidelayer_factory(data=request.POST, prefix="layer") if slide is None else slidelayer_factory(data=request.POST, prefix="layer", instance=slide)
        form = SlideForm(data=request.POST) if slide is None else SlideForm(data=request.POST, instance=slide)

        # Generate Preview
        if prev is not False:
            slidelayerset.is_valid()
            form.is_valid()

            slidelayers = []
            for instance in slidelayerset:
                if instance.cleaned_data.get('DELETE') is False:
                    slidelayer = instance.save(commit=False)
                    slidelayers.append((slidelayer, renderSlideLayer(slidelayer)))

            return render(request, 'slides/sliderender.html', {'slidelayers': slidelayers})

        # Check if evertything is OK
        if slidelayerset.is_valid() and form.is_valid():
            savestatus = True
            # Handle the slide
            newslide = form.save(commit=False)
            if slide is None:
                newslide.owner = request.user
            newslide.save()

            # Handle the slidelayers
            instances = slidelayerset.save(commit=False)
            for obj in slidelayerset.deleted_objects:
                obj.delete()
            for instance in instances:
                instance.slide = newslide
                instance.save()
            request.session['newlysaved'] = True
            return HttpResponseRedirect('/slides/edit/%s/' % newslide.id)

    context = {'layers': Layer.objects.all(), 'form': form, 'layerset': slidelayerset, 'slide': slide, 'savestatus': savestatus}
    return render(request, 'slides/canvas.html', context)


def newLayerJson(request, version_id):
    version = get_object_or_404(Version, pk=version_id)
    data = {"versionid": version.id, "title": version.layer.title, 'short': version.layer.short, "data": {}}
    for control in version.control_set.all():
        data['data'][control.variable_name] = control.default
    return JsonResponse(data, safe=True)

@csrf_exempt
def fieldValidation(request):
    pass


def viewSlide(request, slide_id):
    slide = get_object_or_404(Slide, pk=slide_id)
    slidelayers = []
    for instance in slide.slidelayer_set.all():
        slidelayers.append((instance, renderSlideLayer(instance.version.id, instance.data)))
    return render(request, 'slides/sliderender.html', {'slidelayers': slidelayers})


@csrf_exempt
@vary_on_cookie
def previewLayer(request):
    if request.method == "POST":
        return HttpResponse(renderSlideLayer(request.POST.get('version'), request.POST.get('data', '{}')))


def renderSlideLayer(versionid, data):
    version = Version.objects.get(pk=versionid)
    template = Template(version.content)
    template.globals.update(extra_globals)
    if data == '{}':
        data = {control.variable_name: control.default for control in version.control_set.all()}
    else:
        data = json.loads(data)
    try:
        output = template.render(**data)
    except Exception, e:
        output = str(e)
    return output


@login_required
def cloneSlideshow(request, slideshow_id):
    slideshow = get_object_or_404(Slideshow, pk=slideshow_id)
    # NEED TO VERIFY USER HAS VIEW PERMISSION!
    newslideshow = slideshow.clone(request.user)
    return HttpResponseRedirect('/slideshows/edit/%s/' % newslideshow.id)


@login_required
@vary_on_cookie
def manageSlideshows(request):
    # for now get all official layers and all user layers
    return render(request, 'slideshows/slideshows.html')


@login_required
@vary_on_cookie
def listSlideshows(request, mode):
    slideshows = Slideshow.objects.filter(owner=request.user).order_by('edit_date')
    groups = request.user.contentgroup_set.all() | request.user.owned_groups.all()
    groups = groups.distinct().annotate(c=Count('slideshows')).filter(c__gt=0)
    return render(request, 'slideshows/slideshow_list.html', {'slideshows': slideshows, 'mode': mode, 'groups': groups})


@login_required
@vary_on_cookie
def editSlideshow(request, slideshow_id=None):
    savestatus = None

    slideshow = None if slideshow_id is None else get_object_or_404(Slideshow, pk=slideshow_id)

    # Verify the user can edit this
    if slideshow is not None:
        if not get_item_permissions(request.user, slideshow).get('edit', False):
            raise Http404

    form = SlideshowForm() if slideshow is None else SlideshowForm(instance=slideshow)
    slideset_factory = inlineformset_factory(Slideshow, SlideshowSlide, form=SlideshowSlideForm, can_delete=True, extra=0)
    slideset = slideset_factory(prefix="slide") if slideshow is None else slideset_factory(prefix="slide", instance=slideshow)

    if request.method == "POST":
        form = SlideshowForm(data=request.POST) if slideshow is None else SlideshowForm(data=request.POST, instance=slideshow)
        slideset = slideset_factory(data=request.POST, prefix="slide") if slideshow is None else slideset_factory(data=request.POST, prefix="slide", instance=slideshow)

        if slideset.is_valid() and form.is_valid():
            newslideshow = form.save(commit=False)
            if newslideshow.id is None:
                newslideshow.owner = request.user
            newslideshow.save()

            instances = slideset.save(commit=False)
            for obj in slideset.deleted_objects:
                obj.delete()
            for instance in instances:
                instance.slideshow = newslideshow
                instance.save()
            request.session['newlysaved'] = True
            return HttpResponseRedirect('/slideshows/edit/%s/' % newslideshow.id)

    context = {'slides': Slide.objects.all(), 'form': form, 'slideset': slideset, 'slideshow': slideshow, 'savestatus': savestatus}
    return render(request, 'slideshows/editslideshow.html', context)


def viewSlideshow(request, slideshow_id):
    slideshow = get_object_or_404(Slideshow, pk=slideshow_id)
    first_slide = slideshow.slideshowslide_set.get(order=0)
    return render(request, 'slideshows/slideshowrender.html', {'slideshow': slideshow, 'fs_src': '/slides/view/%s/' % first_slide.slide.id, 'fs_time': first_slide.length * 1000})


def nextSlide(request, slideshow_id, current_index):
    slideshow = get_object_or_404(Slideshow, pk=slideshow_id)
    try:
        next_slide = slideshow.slideshowslide_set.get(order=int(current_index) + 1)
        index = int(current_index) + 1
    except Exception, e:
        print str(e)
        next_slide = slideshow.slideshowslide_set.get(order=0)
        index = 0
    return JsonResponse({'slide': '/slides/view/%s/' % next_slide.slide.id, 'time': next_slide.length * 1000, 'index': index})


###
# Station Management
###
@login_required
@vary_on_cookie
def manageStations(request):
    return render(request, 'stations/coming-soon.html')


###
# Group Management
###
@login_required
@vary_on_cookie
def manageGroups(request):
    return render(request, 'groups/groups.html')


@login_required
@vary_on_cookie
def listGroups(request, mode):
    groups = ContentGroup.objects.filter(owner=request.user).order_by('title')
    other_groups = ContentGroup.objects.filter(members=request.user)
    return render(request, 'groups/group_list.html', {'groups': groups, 'other_groups': other_groups, 'mode': mode})


@login_required
@vary_on_cookie
def editGroup(request, group_id=None):
    group = None if group_id is None else get_object_or_404(ContentGroup, pk=group_id)

    if group is not None:
        perms = {'edit': False, 'add': False, 'remove': False}
        if group.owner == request.user:
            perms = {'edit': True, 'add': True, 'remove': True}
        else:
            membership = group.groupmembership_set.filter(user=request.user).first()
            if membership is not None:
                perms = {'edit': (membership.can_add_users or membership.can_remove_users), 'add': membership.can_add_items}
        if not perms['edit']:
            raise Http404

    form = GroupForm() if group is None else GroupForm(instance=group)
    memberset_factory = inlineformset_factory(ContentGroup, GroupMembership, form=GroupMembershipForm, can_delete=True, extra=0)
    memberset = memberset_factory(prefix="member") if group is None else memberset_factory(prefix="member", instance=group)

    if request.method == "POST":
        print request.POST
        # Alter the post data: Convert all user names to ID numbers and create any users necessary
        adjusted_data = request.POST.copy()
        for key, val in adjusted_data.iteritems():
            if key.startswith('member') and key.endswith('user'):
                user = User.objects.get_or_create(username=val[:8] if len(val) > 8 else val)[0]
                print key, val
                print user
                adjusted_data[key] = str(user.id)
        print adjusted_data
        form = GroupForm(request.POST) if group is None else GroupForm(request.POST, instance=group)
        memberset = memberset_factory(data=adjusted_data, prefix="member") if group is None else memberset_factory(data=adjusted_data, prefix="member", instance=group)

        if form.is_valid() and memberset.is_valid():
            newgroup = form.save(commit=False)
            if newgroup.id is None:
                newgroup.owner = request.user
            newgroup.save()

            instances = memberset.save(commit=False)
            for obj in memberset.deleted_objects:
                obj.delete()
            for instance in instances:
                instance.contentgroup = newgroup
                instance.save()
            request.session['newlysaved'] = True
            return HttpResponseRedirect('/groups/edit/%s/' % newgroup.id)

    for item in memberset:
        item.initial['user'] = User.objects.get(pk=item.initial['user']).username
        print item.initial

    context = {'form': form, 'group': group, 'helper': ControlsInlineHelper(), 'trhelper': ControlsRowHelper(), 'membersform': memberset }
    return render(request, 'groups/editgroup.html', context)

@login_required
@vary_on_cookie
def viewGroup(request, group_id):
    group = get_object_or_404(ContentGroup, pk=group_id)
    # Check if this person can see this group probably
    return render(request, 'groups/view.html', {'group': group, 'mode': 'manage'})


@login_required
@vary_on_cookie
def transferOwnership(request, item, pk):
    # Get the item and identify if the user is the owner
    itemtype = item
    if item == 'slide':
        item = get_object_or_404(Slide, pk=pk)
    elif item == 'layer':
        item = get_object_or_404(Layer, pk=pk)
    elif item == 'slideshow':
        item = get_object_or_404(Slideshow, pk=pk)
    elif item == 'screen':
        item = get_object_or_404(Screen, pk=pk)
    elif item == 'group':
        item = get_object_or_404(ContentGroup, pk=pk)
    else:
        raise Http404

    if request.method == 'POST':
        # Check that we have a valid recipient:
        recipient = request.POST.get('recipient', None)
        if recipient is not None:
            recipient = User.objects.filter(username=recipient.lower()).first()
        if recipient is not None and recipient != request.user:
            # If this is a group we need to do some fancy finagling...
            if itemtype == 'group':
                # If the recipient is already in the group, remove their current ownership...
                membership = item.groupmembership_set.filter(user=recipient).first()
                if membership is not None:
                    membership.delete()
                # Make the current owner a new fully-permissive membership in the group
                GroupMembership.objects.create(can_add_users=True,
                                               can_remove_users=True,
                                               can_use_items=True,
                                               can_add_items=True,
                                               can_remove_items=True,
                                               can_edit_items=True,
                                               contentgroup=item,
                                               user=request.user
                                               )
                if request.POST.get('transfer_all_items', '') == 'true':
                    # Transfer everything
                    # item.layers.filter(owner=request.user).update(owner=recipient)
                    item.screens.filter(owner=request.user).update(owner=recipient)
                    item.slides.filter(owner=request.user).update(owner=recipient)
                    item.slideshows.filter(owner=request.user).update(owner=recipient)
                    item.channels.filter(owner=request.user).update(owner=recipient)
            item.owner = recipient
            item.save()

            return HttpResponse('SUCCESS')
        return HttpResponse('FAILED')

    return render(request, 'groups/transfer.html', {'item': item, 'itemtype': itemtype})


@login_required
@vary_on_cookie
def shareSettings(request, item, pk):
    # Get the item and identify if the user is the owner
    itemtype = item
    if item == 'slide':
        item = get_object_or_404(Slide, pk=pk)
    elif item == 'layer':
        item = get_object_or_404(Layer, pk=pk)
    elif item == 'slideshow':
        item = get_object_or_404(Slideshow, pk=pk)
    elif item == 'screen':
        item = get_object_or_404(Screen, pk=pk)
    else:
        raise Http404

    if request.method == 'POST':
        # First verify that this user can even do this..
        if request.user == item.owner:  # OR WHATEVER OTHER RULES APPLY FOR SHARING THIS SHIT
            # Get all the items in the list and compare them with the groups the item is already in
            oldgroups = set([x.id for x in item.contentgroup_set.all()])
            newgroups = set([int(x) for x in request.POST.getlist('data[]')])
            adding = list(newgroups - oldgroups)
            removing = list(oldgroups - newgroups)
            for groupid in adding:
                # NEED TO VERIFY THAT THE USER CAN DO THESE THINGS PLS
                item.contentgroup_set.add(ContentGroup.objects.get(pk=groupid))
            for groupid in removing:
                item.contentgroup_set.remove(ContentGroup.objects.get(pk=groupid))
            return HttpResponse('SUCCESS')
        raise Http404
    else:
        # Get everything this item is currently shared with, and determine if the user can delete it....
        groups = [{'title': x.title, 'id': x.id, 'candelete': True if item.owner == request.user else False} for x in item.contentgroup_set.all()]
        context = {'item': item, 'itemtype': itemtype, 'groups': json.dumps(groups)}

        return render(request, 'groups/share_modal.html', context)


@login_required
@vary_on_cookie
def manageScreens(request):
    # for now get all official layers and all user layers
    return render(request, 'screens/screens.html')


@login_required
@vary_on_cookie
def listScreens(request):
    screens = Screen.objects.filter(owner=request.user).order_by('title')
    groups = request.user.contentgroup_set.all() | request.user.owned_groups.all()
    groups = groups.distinct().annotate(c=Count('screens')).filter(c__gt=0)
    return render(request, 'screens/screen_list.html', {'screens': screens, 'groups': groups})


@login_required
@vary_on_cookie
def editScreen(request, screen_id=None):
    savestatus = None

    screen = None if screen_id is None else get_object_or_404(Screen, pk=screen_id)
    item = None
    itemtype = None

    if screen is not None:
        linksplit = screen.link.split('/')
        itemtype = linksplit[1]
        print linksplit
        if itemtype == 'slides':
            item = Slide.objects.get(pk=int(linksplit[3]))
        elif itemtype == 'slideshows':
            item = Slideshow.objects.get(pk=int(linksplit[3]))

    # Verify the user can edit this
    if screen is not None:
        if not get_item_permissions(request.user, screen).get('edit', False):
            raise Http404

    form = ScreenForm() if screen is None else ScreenForm(instance=screen)

    if request.method == 'POST':
        form = ScreenForm(request.POST) if screen is None else ScreenForm(request.POST, instance=screen)
        if form.is_valid():
            s = form.save(commit=False)
            if s.id is None:
                s.owner = request.user
            s.save()
            request.session['newlysaved'] = True
            return HttpResponseRedirect('/screens/edit/%s/' % s.id)

    return render(request, 'screens/editscreen.html', {'screen': screen, 'form': form, 'itemtype': itemtype, 'item': item, 'mode': 'manage'})


def viewScreen(request, slug):
    screen = get_object_or_404(Screen, slug=slug)
    now = datetime.now()
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip_address = x_forwarded_for.split(',')[0]
    else:
        ip_address = request.META.get('REMOTE_ADDR')

    screen.last_connect = now
    screen.ip = ip_address
    screen.save()
    return render(request, 'screens/viewscreen.html', {'screen': screen})


def pingScreen(request, slug):
    screen = get_object_or_404(Screen, slug=slug)
    now = datetime.now()
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip_address = x_forwarded_for.split(',')[0]
    else:
        ip_address = request.META.get('REMOTE_ADDR')

    screen.last_connect = now
    screen.ip = ip_address
    screen.save()
    return JsonResponse(screen.link, safe=False)

