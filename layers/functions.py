from layers.models import GroupMembership, ContentGroup


def get_item_permissions(user, item):
    perms = {'own': False, 'share': False, 'edit': False, 'delete': False, 'use': False}
    itemtype = item.__class__.__name__
    # First we shall see if the user owns the item...
    if item is None or item.owner == user:
        perms = dict.fromkeys(perms, True)
        return perms
    # Next see if the user owns any groups this item is in...
    owned = ContentGroup.objects.filter(**{"%ss__in" % itemtype.lower(): [item], 'owner': user}).count()
    if owned:
        perms.update({'edit': True, 'use': True})
        return perms
    # Then we shall see if the item has any memberships affiliated with this user
    memberships = GroupMembership.objects.filter(**{"contentgroup__%ss__in" % itemtype.lower(): [item], 'user': user, 'contentgroup__in': item.contentgroup_set.all()})
    if memberships.filter(can_edit_items=True).count():
        perms.update({'edit': True})
    if memberships.filter(can_use_items=True).count():
        perms.update({'use': True})
    if itemtype == 'Layer' and item.shared:
        perms.update({'use': True})
    return perms