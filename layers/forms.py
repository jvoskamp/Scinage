from django import forms
from models import Control, Layer, Version, Slide, SlideLayer, Slideshow, SlideshowSlide, ContentGroup, GroupMembership, Screen
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Fieldset, HTML
from codemirror import CodeMirrorTextarea

form_show_labels = True


class ControlsInlineHelper(FormHelper):
    def __init__(self, *args, **kwargs):
        super(ControlsInlineHelper, self).__init__(*args, **kwargs)
        self.form_method = 'post'
        self.form_tag = False
        self.render_required_fields = True
        self.template = 'table_inline_template.html'


class ControlsRowHelper(FormHelper):
    def __init__(self, *args, **kwargs):
        super(ControlsRowHelper, self).__init__(*args, **kwargs)
        self.form_method = 'post'
        self.render_required_fields = True
        self.form_show_labels = False
        self.form_tag = False
        self.template = 'tr_template.html'


class ControlForm(forms.ModelForm):
    class Meta:
        model = Control
        fields = ["input_type", "title", "variable_name", "verbose", "default"]
        widgets = {'verbose': forms.TextInput()}


class LayerForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(LayerForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.form_tag = False

    class Meta:
        model = Layer
        fields = ["title", "icon", "short", "description", "shared", "tags"]

class VersionForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(VersionForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.form_tag = False
        self.helper.layout = Layout(Fieldset(
                                    '',
                                    'content',
                                    'publish',
                                    ),
                                    HTML("""<div id="changesmodal" style="z-index:3000;" class="modal fade"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button><h4 class="modal-title">Specify Changes</h4></div><div class="modal-body">"""),
                                    HTML("""<p>Specify your changes here. If you keep the 'publish' button checked and save this layer, the layer will be stored as the current 'stable' version of this layer. A new development layer will be created after that which can be worked on.</p>"""),
                                    Fieldset('', 'changes'),
                                    HTML("""</div><div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">Ok</button></div></div></div></div>""")
                                    )

    class Meta:
        model = Version
        fields = ["content", "publish", "changes"]
        widgets = {"content": CodeMirrorTextarea(mode='htmlmixed', dependencies=('javascript', 'xml', 'css'))}


class SlideForm(forms.ModelForm):
    class Meta:
        model = Slide
        fields = ['title', 'description', 'aspect_ratio']
        widgets = {'description': forms.Textarea(attrs={'rows': 3})}

    def __init__(self, *args, **kwargs):
        super(SlideForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.form_tag = False


class SlideLayerForm(forms.ModelForm):
    class Meta:
        model = SlideLayer
        exclude = ['slide']
        widgets = {'version': forms.HiddenInput()}

    def __init__(self, *args, **kwargs):
        super(SlideLayerForm, self).__init__(*args, **kwargs)
        # Add a self-titled class to each item for easy reference in jquery
        # e.g field "index" will have class "index"
        for key, field in self.fields.iteritems():
            field.widget.attrs['class'] = key


class SlideshowForm(forms.ModelForm):
    class Meta:
        model = Slideshow
        fields = ['title', 'description']

    def __init__(self, *args, **kwargs):
        super(SlideshowForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.form_tag = False


class SlideshowSlideForm(forms.ModelForm):
    class Meta:
        model = SlideshowSlide
        exclude = ['slideshow']
        widgets = {'slide': forms.HiddenInput()}

    def __init__(self, *args, **kwargs):
        super(SlideshowSlideForm, self).__init__(*args, **kwargs)
        # Add a self-titled class to each item for easy reference in jquery
        # e.g field "index" will have class "index"
        for key, field in self.fields.iteritems():
            field.widget.attrs['class'] = key
        self.fields['length'].widget.attrs['class'] += ' form-control'


class GroupForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(GroupForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.form_tag = False

    class Meta:
        model = ContentGroup
        fields = ["title", "description"]


class GroupMembershipForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(GroupMembershipForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.form_tag = False

    class Meta:
        model = GroupMembership
        exclude = ['contentgroup']
        widgets = {'user': forms.TextInput()}


class ScreenForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ScreenForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.form_tag = False

    class Meta:
        model = Screen
        exclude = ['ip', 'slug', 'owner', 'last_connect']
        widgets = {'link': forms.HiddenInput()}
