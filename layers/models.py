from django.db import models
from django.contrib.auth.models import User
from django.core.validators import RegexValidator, ValidationError

from django.db.models.deletion import Collector
from django.db.models.fields.related import ForeignKey

from django.utils.text import slugify

from imagekit.models import ProcessedImageField, ImageSpecField
from imagekit.processors import ResizeToFit, ResizeToFill

from taggit.managers import TaggableManager
from haystack import signals

"""
When a user creates a new layer, it will create a version to go with it.
Anytime a user wants to change what their layer looks like, it will create a new
version for the revision, and (if it is not shared) offer to delete older versions.

A version is simply a list of input types and the jinja2(?) html code to render the object

Input types will each contain html / css / js to add an extra field for users to fill in.
We'll have colorpickers and shit.
"""
variable = RegexValidator(r'^[a-zA-Z]{1}[0-9a-zA-Z_]*$', 'Only alphanumeric characters are allowed. Must start with a letter.')


class UploadedImage(models.Model):
    owner = models.ForeignKey(User)
    creation_date = models.DateTimeField(auto_now_add=True)
    image = ProcessedImageField(upload_to='uploaded_images',
                                processors=[ResizeToFit(1280, 720, False)],
                                format='PNG',
                                options={'quality': 80})
    thumbnail = ImageSpecField(source='image',
                               processors=[ResizeToFit(100, 100)],
                               format='JPEG',
                               options={'quality': 60})


class Layer(models.Model):
    owner = models.ForeignKey(User)
    creation_date = models.DateTimeField(auto_now_add=True)
    shared = models.BooleanField(default=False)
    official = models.BooleanField(default=False)
    title = models.CharField(max_length=255)
    short = models.CharField(max_length=12)
    description = models.TextField()
    tags = TaggableManager()

    # tags
    icon = ProcessedImageField(upload_to='icons',
                               processors=[ResizeToFit(72, 72)],
                               format='PNG',
                               options={'quality': 80},
                               blank=True, null=True)

    def bleeding_edge(self):
        pass

    def latest(self):
        try:
            return self.version_set.filter(publish=True).order_by('-creation_date')[0:1].get()
        except:
            return self.version_set.order_by('-creation_date')[0:1].get()

    def uses(self):
        return SlideLayer.objects.filter(version__layer=self).count()

    def favs(self):
        return self.favoritelayer_set.all().count()

    def clone(self, user):
        newlayer = Layer.objects.get(id=self.id)
        newlayer.id = None
        newlayer.shared = False
        newlayer.official = False
        newlayer.title = "CLONE OF %s" % self.title
        newlayer.owner = user

        newlayer.save()

        # And make the version...
        oldversion = self.latest()
        newversion = self.latest()

        newversion.id = None
        newversion.publish = False
        newversion.changes = ''
        newversion.layer = newlayer
        newversion.save()
        for control in oldversion.control_set.all():
            newcontrol = Control(version=newversion,
                                 title=control.title,
                                 variable_name=control.variable_name,
                                 verbose=control.verbose,
                                 input_type=control.input_type,
                                 default=control.default)
            newcontrol.save()
        return newlayer


class FavoriteLayer(models.Model):
    user = models.ForeignKey(User)
    layer = models.ForeignKey(Layer)


class Version(models.Model):
    layer = models.ForeignKey(Layer)
    content = models.TextField()
    creation_date = models.DateTimeField(auto_now_add=True)
    edit_date = models.DateTimeField(auto_now=True, blank=True, null=True)
    changes = models.TextField(blank=True)
    publish = models.BooleanField(default=False)
    # every edit after a publish will be a new version!

    last_edit = models.ForeignKey(User, blank=True, null=True, related_name='version_last_edit')

    def save(self, *args, **kwargs):
        super(Version, self).save(*args, **kwargs)
        if self.publish is True:
            # We're going to do this by hand because stackoverflow only has answers for ancient versions of django
            # and I spent too bloody long trying to write a general duplicator and got only nonsense error messages
            # Duplicate the object to make a new HEAD version.
            new = Version(layer=self.layer, content=self.content, publish=False, last_edit=self.last_edit)
            new.save()
            # Get all the controls too
            for control in self.control_set.all():
                newcontrol = Control(version=new,
                                     title=control.title,
                                     variable_name=control.variable_name,
                                     verbose=control.verbose,
                                     input_type=control.input_type,
                                     default=control.default)
                newcontrol.save()


class InputType(models.Model):
    title = models.CharField(max_length=255)
    html = models.TextField()
    validator = models.CharField(max_length=255)
    validator_error = models.CharField(max_length=255)

    def __unicode__(self):
        return self.title


class Control(models.Model):
    version = models.ForeignKey(Version)
    title = models.CharField(max_length=255)
    variable_name = models.CharField(max_length=18)
    verbose = models.TextField(blank=True)
    input_type = models.ForeignKey(InputType)
    default = models.CharField(max_length=255)


class Slide(models.Model):
    owner = models.ForeignKey(User)
    title = models.CharField(max_length=255)
    description = models.TextField(blank=True)
    data = models.TextField(blank=True)
    creation_date = models.DateTimeField(auto_now_add=True)
    edit_date = models.DateTimeField(auto_now=True, blank=True, null=True)

    last_edit = models.ForeignKey(User, null=True, related_name='slide_edit_user', blank=True)

    # Advanced Settings
    aspect_ratio = models.CharField(max_length=8, default="16x9",
                                    validators=[RegexValidator(
                                    regex='^(\d+x\d+)$',
                                    message='Must be in the form WidthxHeight',)])

    def clone(self, user):
        # Clone the original...
        newslide = Slide.objects.get(pk=self.id)
        newslide.pk = None
        newslide.owner = user
        newslide.title = 'CLONE OF %s' % self.title
        newslide.save()

        # And get all the slidelayers
        for sl in self.slidelayer_set.all():
            sl.pk = None
            sl.slide = newslide
            sl.save()
        return newslide


class SlideLayer(models.Model):
    slide = models.ForeignKey(Slide)
    x = models.FloatField(default=40)
    y = models.FloatField(default=40)
    width = models.FloatField(default=20)
    height = models.FloatField(default=20)
    title = models.CharField(max_length=100)
    index = models.IntegerField()
    version = models.ForeignKey(Version)
    data = models.TextField(blank=True)

    def control(self):
        return self.version.control


class Slideshow(models.Model):
    owner = models.ForeignKey(User)
    title = models.CharField(max_length=255)
    description = models.TextField(blank=True)
    creation_date = models.DateTimeField(auto_now_add=True)
    edit_date = models.DateTimeField(auto_now=True, blank=True, null=True)

    last_edit = models.ForeignKey(User, null=True, related_name='slideshow_last_edit', blank=True)

    def clone(self, user):
        # Clone the original...
        newslideshow = Slideshow.objects.get(pk=self.id)
        newslideshow.pk = None
        newslideshow.owner = user
        newslideshow.title = 'CLONE OF %s' % self.title
        newslideshow.save()

        # And get all the slideshowslides
        for sl in self.slideshowslide_set.all():
            sl.pk = None
            sl.slideshow = newslideshow
            sl.save()
        return newslideshow


class SlideshowSlide(models.Model):
    slideshow = models.ForeignKey(Slideshow)
    slide = models.ForeignKey(Slide)
    length = models.IntegerField(default=30)
    order = models.IntegerField()


class Channel(models.Model):
    owner = models.ForeignKey(User)
    title = models.CharField(max_length=255)
    description = models.TextField(blank=True)
    creation_date = models.DateTimeField(auto_now_add=True)
    edit_date = models.DateTimeField(auto_now=True, blank=True, null=True)

    last_edit = models.ForeignKey(User, null=True, related_name='channel_last_edit', blank=True)


class ChannelItem(models.Model):
    channel = models.ForeignKey(Channel)

    # Is it a slide or a slideshow? Form requires one or the other to be true
    slide = models.ForeignKey(Slide, blank=True, null=True)
    slideshow = models.ForeignKey(Slideshow, blank=True, null=True)

    priority = models.IntegerField(default=1)
    start_date = models.DateTimeField()
    end_date = models.DateTimeField()
    repeat = models.CharField(max_length=200)


class Screen(models.Model):
    owner = models.ForeignKey(User)
    title = models.CharField(max_length=255, unique=True, help_text="If title changes after initial configuration, URL will change.")
    description = models.TextField(blank=True)
    slug = models.SlugField(max_length=255)

    link = models.CharField(max_length=255)
    ip = models.GenericIPAddressField(blank=True, null=True)
    last_connect = models.DateTimeField(blank=True, null=True)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        super(Screen, self).save(*args, **kwargs)


class ContentGroup(models.Model):
    owner = models.ForeignKey(User, related_name='owned_groups')
    members = models.ManyToManyField(User, through='GroupMembership')
    title = models.CharField(max_length=255)
    description = models.TextField(max_length=255)

    layers = models.ManyToManyField(Layer, blank=True)
    slides = models.ManyToManyField(Slide, blank=True)
    slideshows = models.ManyToManyField(Slideshow, blank=True)
    channels = models.ManyToManyField(Channel, blank=True)
    screens = models.ManyToManyField(Screen, blank=True)


class GroupMembership(models.Model):
    user = models.ForeignKey(User)
    contentgroup = models.ForeignKey(ContentGroup)

    # Permissions
    can_add_users = models.BooleanField(default=False)
    can_remove_users = models.BooleanField(default=False)
    can_use_items = models.BooleanField(default=False)
    can_add_items = models.BooleanField(default=False)
    can_remove_items = models.BooleanField(default=False)
    can_edit_items = models.BooleanField(default=False)



# SIGNALS
class LayerSignalProcessor(signals.BaseSignalProcessor):
    def setup(self):
        models.signals.post_save.connect(self.handle_save, sender=Layer)
        models.signals.post_delete.connect(self.handle_delete, sender=Layer)

    def teardown(self):
        models.signals.post_save.disconnect(self.handle_save, sender=Layer)
        models.signals.post_delete.disconnect(self.handle_delete, sender=Layer)