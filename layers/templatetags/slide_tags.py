from django import template
from layers.functions import get_item_permissions
from layers.models import FavoriteLayer
register = template.Library()


@register.inclusion_tag('layers/layer_list_single.html', takes_context=True)
def layer_item(context, layer, mode='ll'):
    return {
        'user': context['user'],
        'layer': layer,
        'mode': mode,
        'perms': get_item_permissions(context['user'], layer)
    }


@register.inclusion_tag('layers/layer_list_fav.html', takes_context=True)
def layer_item_fav(context, layer, mode='manage'):
    return {
        'user': context['user'],
        'layer': layer,
        'mode': mode,
        'favorite': FavoriteLayer.objects.filter(user=context['user'], layer=layer).count(),
        'perms': get_item_permissions(context['user'], layer)
    }


@register.inclusion_tag('screens/screen_item.html', takes_context=True)
def screen_item(context, screen):
    return {
        'user': context['user'],
        'screen': screen,
        'perms': get_item_permissions(context['user'], screen)
    }


@register.inclusion_tag('slides/slide_list_single_new.html', takes_context=True)
def slide_item(context, slide):
    # Determine if a user is able to edit / share here
    return {
        'user': context['user'],
        'slide': slide,
        'mode': context['mode'],
        'perms': get_item_permissions(context['user'], slide)
    }


@register.inclusion_tag('groups/group_list_single.html', takes_context=True)
def group_item(context, group):
    perms = {'edit': False, 'add': False, 'remove': False}
    if group.owner == context['user']:
        perms = {'edit': True, 'add': True, 'remove': True}
    else:
        membership = group.groupmembership_set.filter(user=context['user']).first()
        if membership is not None:
            perms = {'edit': (membership.can_add_users or membership.can_remove_users), 'add': membership.can_add_items}
    return {
        'user': context['user'],
        'group': group,
        'perms': perms,
        'mode': context['mode']
    }


@register.inclusion_tag('slideshows/slideshow_list_single_new.html', takes_context=True)
def slideshow_item(context, slideshow):
    return {
        'user': context['user'],
        'slideshow': slideshow,
        'mode': context['mode'],
        'perms': get_item_permissions(context['user'], slideshow)
    }


@register.inclusion_tag('slides/slidelayers_template.html', takes_context=True)
def slidelayers_all(context):
    return {
        'layerset': context['layerset']
    }


@register.inclusion_tag('slides/slidelayer_template.html')
def slidelayers_single(form):
    return{
        'form': form
    }


@register.inclusion_tag('slideshows/slideshowslides_template.html', takes_context=True)
def slideshowslides_all(context):
    return {
        'slideset': context['slideset']
    }


@register.inclusion_tag('slideshows/slideshowslides_single_template.html')
def slideshowslides_single(form):
    return{
        'form': form
    }

@register.inclusion_tag('groups/sharegroup_template.html')
def sharegroups_single(form):
    return{
        'form': form
    }



##### Filters

@register.simple_tag(takes_context=True)
def canadd(context, group):
    print group.title
    if group.owner == context['user']:
        return True
    elif group.groupmembership_set.filter(user=context['user']).first().can_add_items is True:
        return True
    else:
        return False