# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('layers', '0006_auto_20151027_0121'),
    ]

    operations = [
        migrations.RenameField(
            model_name='slideshowslide',
            old_name='s_order',
            new_name='order',
        ),
    ]
