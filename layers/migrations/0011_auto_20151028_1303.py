# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('layers', '0010_version_publish'),
    ]

    operations = [
        migrations.CreateModel(
            name='ContentGroup',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255)),
                ('description', models.TextField(max_length=255)),
            ],
        ),
        migrations.RemoveField(
            model_name='layer',
            name='ro_access',
        ),
        migrations.RemoveField(
            model_name='layer',
            name='rw_access',
        ),
        migrations.RemoveField(
            model_name='slide',
            name='ro_access',
        ),
        migrations.RemoveField(
            model_name='slide',
            name='rw_access',
        ),
        migrations.RemoveField(
            model_name='slideshow',
            name='ro_access',
        ),
        migrations.RemoveField(
            model_name='slideshow',
            name='rw_access',
        ),
        migrations.RemoveField(
            model_name='station',
            name='ro_access',
        ),
        migrations.RemoveField(
            model_name='station',
            name='rw_access',
        ),
        migrations.AddField(
            model_name='contentgroup',
            name='layers',
            field=models.ManyToManyField(to='layers.Layer', blank=True),
        ),
        migrations.AddField(
            model_name='contentgroup',
            name='owner',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='contentgroup',
            name='ro_users',
            field=models.ManyToManyField(related_name='ro_users', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='contentgroup',
            name='rw_users',
            field=models.ManyToManyField(related_name='rw_users', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='contentgroup',
            name='slides',
            field=models.ManyToManyField(to='layers.Slide', blank=True),
        ),
        migrations.AddField(
            model_name='contentgroup',
            name='slideshows',
            field=models.ManyToManyField(to='layers.Slideshow', blank=True),
        ),
        migrations.AddField(
            model_name='contentgroup',
            name='stations',
            field=models.ManyToManyField(to='layers.Station', blank=True),
        ),
    ]
