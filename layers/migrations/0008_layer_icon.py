# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import imagekit.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('layers', '0007_auto_20151027_0127'),
    ]

    operations = [
        migrations.AddField(
            model_name='layer',
            name='icon',
            field=imagekit.models.fields.ProcessedImageField(null=True, upload_to=b'icons', blank=True),
        ),
    ]
