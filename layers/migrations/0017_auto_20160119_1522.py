# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import taggit.managers
import imagekit.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('taggit', '0002_auto_20150616_2121'),
        ('layers', '0016_uploadedimage'),
    ]

    operations = [
        migrations.AddField(
            model_name='layer',
            name='tags',
            field=taggit.managers.TaggableManager(to='taggit.Tag', through='taggit.TaggedItem', help_text='A comma-separated list of tags.', verbose_name='Tags'),
        ),
        migrations.AlterField(
            model_name='uploadedimage',
            name='image',
            field=imagekit.models.fields.ProcessedImageField(upload_to=b'uploaded_images'),
        ),
    ]
