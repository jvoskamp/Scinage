# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('layers', '0013_auto_20151203_2136'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='contentgroup',
            name='owner',
        ),
        migrations.AddField(
            model_name='contentgroup',
            name='owner',
            field=models.ForeignKey(related_name='owned_groups', default=0, to=settings.AUTH_USER_MODEL),
            preserve_default=False,
        ),
    ]
