# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('layers', '0003_slide'),
    ]

    operations = [
        migrations.CreateModel(
            name='SlideLayer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('x', models.FloatField(default=40)),
                ('y', models.FloatField(default=40)),
                ('width', models.FloatField(default=20)),
                ('height', models.FloatField(default=20)),
                ('title', models.CharField(max_length=100)),
                ('index', models.IntegerField()),
                ('data', models.TextField(blank=True)),
                ('slide', models.ForeignKey(to='layers.Slide')),
                ('version', models.ForeignKey(to='layers.Version')),
            ],
        ),
    ]
