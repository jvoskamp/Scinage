# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('layers', '0009_auto_20151027_2359'),
    ]

    operations = [
        migrations.AddField(
            model_name='version',
            name='publish',
            field=models.BooleanField(default=False),
        ),
    ]
