# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('layers', '0017_auto_20160119_1522'),
    ]

    operations = [
        migrations.AddField(
            model_name='slide',
            name='aspect_ratio',
            field=models.CharField(default=b'16x9', max_length=8, validators=[django.core.validators.RegexValidator(regex=b'^(\\d+x\\d+)$', message=b'Must be in the form WidthxHeight')]),
        ),
    ]
