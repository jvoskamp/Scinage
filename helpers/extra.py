import tweepy
import json
import requests
from django.conf import settings


# Get some tweets
def get_tweets(q, num=20):
    auth = tweepy.OAuthHandler(*settings.TWITTER_KEYS)
    auth.set_access_token(*settings.TWITTER_TOKEN_KEYS)
    try:
        api = tweepy.API(auth)
        search = api.search(q, rpp=num)
        tweets = []
        for tweet in search:
            tweets.append(tweet._json)
        return json.dumps(tweets)
    except:
        return "[]"


def get_request(url):
    try:
        data = json.dumps(requests.get(url).text)
    except Exception, e:
        print str(e)
        data = "FAILED"
    print data
    if data.startswith('"'):
        data = data[1:-1]
    return data


def jquery(version='1.12.0'):
    return "<script src='/static/js/jquery-%s.min.js'></script>" % version

extra_globals = {'get_tweets': get_tweets, 'get': get_request, 'jquery': jquery}