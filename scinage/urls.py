"""scinage URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^accounts/login/$', 'cas.views.login', name='login'),
    url(r'^accounts/logout/$', 'cas.views.logout', name='logout'),


    url(r'^$', 'layers.views.frontpage', name="home"),
    url(r'^uploadimage/$', 'layers.views.ajaximage', name="upload_image"),
    # Layer Editing
    url(r'^extracontrols/$', 'layers.views.controlsExtra', name="extra_controls"),

    url(r'^layers/search/', include('haystack.urls')),
    url(r'^layers/$', 'layers.views.manageLayers', name="layers"),
    url(r'^layers/list/(?P<mode>\w+)/$', 'layers.views.listLayers', name="list_layers"),
    url(r'^layers/new/$', 'layers.views.editLayer', name="new_layer"),
    url(r'^layers/clone/(?P<layer_id>\d+)/$', 'layers.views.cloneLayer', name="clone_layer"),
    url(r'^layers/edit/(?P<layer_id>\d+)/$', 'layers.views.editLayer', name="edit_layer"),
    url(r'^layers/delete/(?P<layer_id>\d+)/$', 'layers.views.editLayer', name="delete_layer"),
    url(r'^layers/view/(?P<layer_id>\d+)/$', 'layers.views.editLayer', name="view_layer"),
    url(r'^layers/editorinput/(?P<id>\d+)/$', 'layers.views.loadInput'),
    url(r'^layers/favlayer/$', 'layers.views.toggleLayerFav', name='favlayer'),

    # Slide Editing
    url(r'^slides/$', 'layers.views.manageSlides', name="slides"),
    url(r'^slides/list/(?P<mode>\w+)/$', 'layers.views.listSlides', name="list_slides"),
    url(r'^slides/new/$', 'layers.views.editSlide', name="new_slide"),
    url(r'^slides/clone/(?P<slide_id>\d+)/$', 'layers.views.cloneSlide', name="clone_slide"),
    url(r'^slides/edit/(?P<slide_id>\d+)/$', 'layers.views.editSlide', name="edit_slide"),
    url(r'^slides/delete/(?P<slide_id>\d+)/$', 'layers.views.editSlide', name="delete_slide"),
    url(r'^slides/layersettings/(?P<version_id>\d+)/$', 'layers.views.layerSettings'),
    url(r'^slides/layerjson/new/(?P<version_id>\d+)/$', 'layers.views.newLayerJson'),
    url(r'^slides/layerpreview/$', 'layers.views.previewLayer'),

    # Slide Viewing
    url(r'^slides/view/(?P<slide_id>\d+)/$', 'layers.views.viewSlide', name="view_slide"),


    # Slideshow Viewing
    url(r'^slideshows/$', 'layers.views.manageSlideshows', name="slideshows"),
    url(r'^slideshows/list/(?P<mode>\w+)/$', 'layers.views.listSlideshows', name="list_slideshows"),
    url(r'^slideshows/new/$', 'layers.views.editSlideshow', name="new_slideshow"),
    url(r'^slideshows/clone/(?P<slideshow_id>\d+)/$', 'layers.views.cloneSlideshow', name="clone_slideshow"),

    url(r'^slideshows/edit/(?P<slideshow_id>\d+)/$', 'layers.views.editSlideshow', name="edit_slideshow"),
    url(r'^slideshows/delete/(?P<slideshow_id>\d+)/$', 'layers.views.editSlideshow', name="delete_slideshow"),
    url(r'^slideshows/nextslide/(?P<slideshow_id>\d+)/(?P<current_index>\d+)/$', 'layers.views.nextSlide'),
    url(r'^slideshows/view/(?P<slideshow_id>\d+)/$', 'layers.views.viewSlideshow', name="view_slideshow"),

    # Screen Viewing
    url(r'^screens/$', 'layers.views.manageScreens', name="screens"),
    url(r'^screens/list/$', 'layers.views.listScreens', name="list_screens"),
    url(r'^screens/new/$', 'layers.views.editScreen', name="new_screen"),
    url(r'^screens/edit/(?P<screen_id>\d+)/$', 'layers.views.editScreen', name="edit_screen"),
    url(r'^screens/delete/(?P<screen_id>\d+)/$', 'layers.views.editScreen', name="delete_screen"),
    url(r'^screens/view/(?P<slug>[-\w\d]+)/$', 'layers.views.viewScreen', name="view_screen"),
    url(r'^screens/ping/(?P<slug>[-\w\d]+)/$', 'layers.views.pingScreen', name="ping_screen"),


    # Station Stuff
    url(r'^stations/$', 'layers.views.manageStations', name="stations"),

    # Group stuff
    url(r'^groups/$', 'layers.views.manageGroups', name="groups"),
    url(r'^groups/list/(?P<mode>\w+)/$', 'layers.views.listGroups', name="list_groups"),
    url(r'^groups/new/$', 'layers.views.editGroup', name="new_group"),
    url(r'^groups/edit/(?P<group_id>\d+)/$', 'layers.views.editGroup', name="edit_group"),
     url(r'^groups/view/(?P<group_id>\d+)/$', 'layers.views.viewGroup', name="view_group"),
    url(r'^groups/delete/(?P<group_id>\d+)/$', 'layers.views.editGroup', name="delete_group"),

    # Sharing
    url(r'^share/(?P<item>\w+)/(?P<pk>\d+)/$', 'layers.views.shareSettings', name="sharesettings"),
    url(r'^transfer/(?P<item>\w+)/(?P<pk>\d+)/$', 'layers.views.transferOwnership', name="transfer"),

]

from django.conf import settings
from django.conf.urls.static import static

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) + static('/media/', document_root=settings.MEDIA_ROOT)