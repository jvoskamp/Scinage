# Small context processor...
def newlysaved(request):
    if request.session.pop('newlysaved', False):
        return {'newlysaved': True}
    else:
        return {}
